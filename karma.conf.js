module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'fantastic-box-co/bower_components/angular/angular.js',
      'fantastic-box-co/bower_components/angular-route/angular-route.js',
      'fantastic-box-co/bower_components/angular-mocks/angular-mocks.js',
      'fantastic-box-co/bower_components/lodash/lodash.js',
      'fantastic-box-co/components/angular-lodash.js',
      'fantastic-box-co/order/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
