'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {

  it('should automatically redirect to /order when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/order");
  });

  describe('order', function() {

    beforeEach(function() {
      browser.get('index.html#/order');
    });

    it('should render order when user navigates to /order', function() {
      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 1 - Dimensions & Quantity');
    });

    it('should present the total cost screen', function() {
      var height = element(by.model('orderVm.order.dimensions.height')),
          width = element(by.model('orderVm.order.dimensions.width')),
          length = element(by.model('orderVm.order.dimensions.length')),
          quantity = element(by.model('orderVm.order.quantity')),
          nextButton = element(by.css('.btn-next'));

      width.clear();
      width.sendKeys('1');
      height.clear();
      height.sendKeys('1');
      length.clear();
      length.sendKeys('1');
      quantity.clear();
      quantity.sendKeys('1');
      nextButton.click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 2 - Cardboard Grade');

      element(by.id('radio-1')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 3 - Print Quality');

      element(by.id('radio-1')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 4 - Optional Extras');

      element(by.id('check-2')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Total Cost');

      var errors = element(by.id('validation-errors'));
      expect(errors.isPresent()).toBeFalsy();

    });

    it('should show validation error if Grade C and area box > 2 and should correct selection', function() {
      var height = element(by.model('orderVm.order.dimensions.height')),
        width = element(by.model('orderVm.order.dimensions.width')),
        length = element(by.model('orderVm.order.dimensions.length')),
        quantity = element(by.model('orderVm.order.quantity')),
        nextButton = element(by.css('.btn-next'));

      width.clear();
      width.sendKeys('1');
      height.clear();
      height.sendKeys('1');
      length.clear();
      length.sendKeys('1');
      quantity.clear();
      quantity.sendKeys('1');
      nextButton.click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 2 - Cardboard Grade');

      element(by.id('radio-3')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 3 - Print Quality');

      element(by.id('radio-1')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 4 - Optional Extras');

      element(by.id('check-1')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Total Cost');

      var errors = element(by.id('validation-errors'));
      expect(errors.isPresent());

      expect(element(by.id('error-message')).getText()).toContain('The "Grade C" cardboard option is not available');

      // User gos to step 2 and changes to Grade C
      element(by.css('.step-2')).click();
      element(by.id('radio-2')).click();
      element(by.css('.btn-next')).click();
      element(by.css('.btn-next')).click();
      element(by.css('.btn-next')).click();

      var errors = element(by.id('validation-errors'));
      expect(errors.isPresent()).toBeFalsy();

    });

    it('should show error if grade not A and has extra reinforced bottom and should correct selection', function() {
      var height = element(by.model('orderVm.order.dimensions.height')),
        width = element(by.model('orderVm.order.dimensions.width')),
        length = element(by.model('orderVm.order.dimensions.length')),
        quantity = element(by.model('orderVm.order.quantity')),
        nextButton = element(by.css('.btn-next'));

      width.clear();
      width.sendKeys('1');
      height.clear();
      height.sendKeys('1');
      length.clear();
      length.sendKeys('1');
      quantity.clear();
      quantity.sendKeys('1');
      nextButton.click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 2 - Cardboard Grade');

      element(by.id('radio-2')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 3 - Print Quality');

      element(by.id('radio-1')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Step 4 - Optional Extras');

      element(by.id('check-2')).click();
      element(by.css('.btn-next')).click();

      expect(element.all(by.css('h2')).first().getText()).toMatch('Total Cost');

      var errors = element(by.id('validation-errors'));
      expect(errors.isPresent());

      expect(element(by.id('error-message')).getText()).toContain('The "Reinforced bottom" option is only available ' +
        'for Grade A cardboards');

      // User goes to step 2 and changes to Grade A
      element(by.css('.step-2')).click();
      element(by.id('radio-1')).click();
      element(by.css('.btn-next')).click();
      element(by.css('.btn-next')).click();
      element(by.css('.btn-next')).click();

      var errors = element(by.id('validation-errors'));
      expect(errors.isPresent()).toBeFalsy();

    });

  });

});
