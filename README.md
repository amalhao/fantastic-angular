# Order Webapp for AngularJS apps

This project is a quick example of an Webapp coded in AngularJS with Unit and End-to-end tests. 

The structure for this code is based on the angular-seed, that offers a great starting point for AngularJS projects and helps to configure the test suite.

I kept the code comments to a minimum, giving more focus to the appropriate function naming.

## Getting Started

To get you started you can simply install the dependencies:

### Install Dependencies

```
npm install
```

### Run the Application

```
npm start
```

Now browse to the app at `http://localhost:4000/fantastic-box-co`.

## Directory Layout

```
app/                    --> all of the source files for the application
  css/                  --> css folder
    application.css     --> default stylesheet
  components/           --> all application specific modules
    angular-lodash.js   --> lodash module for angular
  order/                --> the order view template and logic
    order.html          --> the partial template
    order.js            --> the controller logic
    order_test.js       --> tests of the controller
  application.js        --> main application module
  favicon.html          --> favicon used by our app
  index.html            --> app layout file (the main html template file of the app)
karma.conf.js           --> config file for running unit tests with Karma
e2e-tests/              --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
```

## Testing

Unit tests and End to End tests. For both type of tests the application needs to be running.

```
npm start
```

### Running Unit Tests

```
npm test
```

For single tests use:

```
npm run test-single-run
```


### End to end testing

```
npm run protractor
```

### Options

This application can be run in Dynamic total price mode, so the user can keep track of the order value at all times. 
To do that you need to change the line in the order/order.js file

```
.constant('DYNAMIC_TOTAL_PRICE', false)
```

to

```
.constant('DYNAMIC_TOTAL_PRICE', true)
```

Enjoy!

### Author

Alexandre Malhao