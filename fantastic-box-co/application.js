(function() {

  'use strict';

  console.info([
    '=========================================',
    '*** FantasticBoxCo is now working :)  ***',
    '=========================================',
  ].join("\n"));

  // Declare app level module which depends on views
  angular.module('myApp', ['ngRoute', 'myApp.order', 'lodash'])

  /* Route configuration - for the sake of this test all possible paths lead to /order */
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/order'});
  }]);

})();
