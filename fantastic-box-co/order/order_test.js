'use strict';

describe('myApp.order module', function() {

  var controller;

  beforeEach(function() {
    controller = {};

    module('myApp.order');
    module('lodash');

    inject(function($controller) {
      controller = $controller;
    });
  });

  describe('order controller', function(){

    var orderCtrl = {};

    beforeEach(function() {
      orderCtrl = controller('Order');
    });

    it('should be defined', function() {

      expect(orderCtrl).toBeDefined();

    });

    it('should contain all order steps with correct values', function() {

      expect(orderCtrl.steps.dimensionsAndQuantity).toBe(1);
      expect(orderCtrl.steps.cardboardGrade).toBe(2);
      expect(orderCtrl.steps.printQuality).toBe(3);
      expect(orderCtrl.steps.optionalExtras).toBe(4);
      expect(orderCtrl.steps.totalCost).toBe(5);

    });

    it('should contain max box area set to 2 m^2', function() {

      expect(orderCtrl.maxAreaForGradeC).toBe(2);

    });

    it('should contain dynamic total price', function() {

      expect(orderCtrl.dynamicTotalPrice).toBe(false);

    });

    it('should contain order options', function() {

      expect(orderCtrl.options).toBeDefined();
      expect(orderCtrl.options.grades).toBeDefined();
      expect(orderCtrl.options.printings).toBeDefined();
      expect(orderCtrl.options.extras).toBeDefined();

      var gradesArray = orderCtrl.options.grades,
          printingsArray = orderCtrl.options.printings,
          extrasArray = orderCtrl.options.extras;

      expect(extrasArray.length).toBe(2);
      expect(printingsArray.length).toBe(5);
      expect(gradesArray.length).toBe(3);

    });

    it('should contain the current step set to dimensionsAndQuantity', function() {

      expect(orderCtrl.currentStep).toBe(orderCtrl.steps.dimensionsAndQuantity);

    });

    it('should contain order object and all initial values', function() {

      expect(orderCtrl.order).toBeDefined();
      expect(orderCtrl.order.dimensions.width).toBe(0.01);
      expect(orderCtrl.order.dimensions.width).toBe(0.01);
      expect(orderCtrl.order.dimensions.height).toBe(0.01);
      expect(orderCtrl.order.dimensions.length).toBe(0.01);
      expect(orderCtrl.order.boxArea).toBe(0);
      expect(orderCtrl.order.quantity).toBe(0);
      expect(orderCtrl.order.grade).toBe('');
      expect(orderCtrl.order.printing).toBe('');
      expect(orderCtrl.order.price.grade).toBe(0);
      expect(orderCtrl.order.price.printing).toBe(0);
      expect(orderCtrl.order.price.extras).toBe(0);
      expect(orderCtrl.order.price.discount).toBe(0);
      expect(orderCtrl.order.price.total).toBe(0);

      var extrasArray = orderCtrl.order.extras;

      expect(extrasArray.length).toBe(0);

    });

    it('should contain all the necessary view model functions', function() {

      expect(typeof orderCtrl.showStep).toBe('function');
      expect(typeof orderCtrl.nextStep).toBe('function');
      expect(typeof orderCtrl.previousStep).toBe('function');
      expect(typeof orderCtrl.updateBoxArea).toBe('function');
      expect(typeof orderCtrl.updateOrderGradePrintingExtrasPrices).toBe('function');
      expect(typeof orderCtrl.updateOrderGradePrice).toBe('function');
      expect(typeof orderCtrl.updateOrderPrintingPrice).toBe('function');
      expect(typeof orderCtrl.toggleExtras).toBe('function');
      expect(typeof orderCtrl.updateTotalPrice).toBe('function');
      expect(typeof orderCtrl.showExtras).toBe('function');
      expect(typeof orderCtrl.validateOptions).toBe('function');

    });
  });

  describe('order box area', function() {

    var orderCtrl = {};

    beforeEach(function() {
      orderCtrl = controller('Order');
    });

    it('for 1h x 1l x 1w box area should 6 m²', inject(function (_) {

      orderCtrl.order.dimensions.height = 1;
      orderCtrl.order.dimensions.width = 1;
      orderCtrl.order.dimensions.length = 1;

      orderCtrl.updateBoxArea();

      var boxArea = orderCtrl.order.boxArea;

      expect(boxArea).toBe(6.00);

    }));

    it('for 0.5h x 0.3l x 0.7w box area should 1.42 m²', inject(function (_) {

      orderCtrl.order.dimensions.height = 0.5;
      orderCtrl.order.dimensions.width = 0.3;
      orderCtrl.order.dimensions.length = 0.7;

      orderCtrl.updateBoxArea();

      var boxArea = orderCtrl.order.boxArea;

      expect(boxArea).toBe(1.42);

    }));

    it('for 0h x 0l x 0w box area should 0 m²', inject(function (_) {

      orderCtrl.order.dimensions.height = 0;
      orderCtrl.order.dimensions.width = 0;
      orderCtrl.order.dimensions.length = 0;

      orderCtrl.updateBoxArea();

      var boxArea = orderCtrl.order.boxArea;

      expect(boxArea).toBe(0);

    }));
  });

  describe('order for box area 6m^2, quantity 10 and', function() {

    var orderCtrl = {};

    beforeEach(function() {
      orderCtrl = controller('Order');
      orderCtrl.order.boxArea = 6;
      orderCtrl.order.quantity = 10;
    });

    it('grade c should be 12', inject(function (_) {

      orderCtrl.order.grade = 'A';
      orderCtrl.updateOrderGradePrice();

      expect(orderCtrl.order.price.grade).toBe(12);

    }));

    it('grade b should be 6', inject(function (_) {

      orderCtrl.order.grade = 'B';
      orderCtrl.updateOrderGradePrice();

      expect(orderCtrl.order.price.grade).toBe(6);

    }));

    it('grade c should be 3', inject(function (_) {

      orderCtrl.order.grade = 'C';
      orderCtrl.updateOrderGradePrice();

      expect(orderCtrl.order.price.grade).toBe(3);

    }));

    it('printing 3 colour printing should be 12', inject(function (_) {

      orderCtrl.order.printing = '3 colour printing';
      orderCtrl.updateOrderPrintingPrice();

      expect(orderCtrl.order.price.printing).toBe(12);

    }));

    it('printing 2 colour printing should be 6', inject(function (_) {

      orderCtrl.order.printing = '2 colour printing';
      orderCtrl.updateOrderPrintingPrice();

      expect(orderCtrl.order.price.printing).toBe(6);

    }));

    it('printing black only printing should be 3', inject(function (_) {

      orderCtrl.order.printing = 'Black only printing';
      orderCtrl.updateOrderPrintingPrice();

      expect(orderCtrl.order.price.printing).toBe(3);

    }));

    it('printing no printing (plain cardboard) should be 0', inject(function (_) {

      orderCtrl.order.printing = 'No printing (plain cardboard)';
      orderCtrl.updateOrderPrintingPrice();

      expect(orderCtrl.order.price.printing).toBe(0);

    }));

    it('printing FantasticBoxCo should be 0', inject(function (_) {

      orderCtrl.order.printing = 'FantasticBoxCo';
      orderCtrl.updateOrderPrintingPrice();

      expect(orderCtrl.order.price.printing).toBe(0);
      expect(orderCtrl.order.price.discount).toBe(0.05);

    }));

    it('no extras should be 0', inject(function (_) {

      orderCtrl.order.extras = [];
      orderCtrl.updateOrderExtrasPrice();

      expect(orderCtrl.order.price.extras).toBe(0);

    }));

    it('handles should be ', inject(function (_) {

      orderCtrl.order.extras = ['Handles'];
      orderCtrl.updateOrderExtrasPrice();

      expect(orderCtrl.order.price.extras).toBe(1);

    }));

    it('reinforced bottom should be ', inject(function (_) {

      orderCtrl.order.extras = ['Reinforced bottom'];
      orderCtrl.updateOrderExtrasPrice();

      expect(orderCtrl.order.price.extras).toBe(0.5);

    }));

    it('reinforced bottom and handles should be ', inject(function (_) {

      orderCtrl.order.extras = ['Reinforced bottom', 'Handles'];
      orderCtrl.updateOrderExtrasPrice();

      expect(orderCtrl.order.price.extras).toBe(1.5);

    }));

  });

  describe('order total price', function() {

    var orderCtrl = {};

    beforeEach(function() {
      orderCtrl = controller('Order');
      orderCtrl.order.price.grade = 12;
      orderCtrl.order.price.printing = 12;
      orderCtrl.order.price.extras = 1.5;
    });

    it('without discount should be 25.5', function () {

      orderCtrl.updateTotalPrice();
      expect(orderCtrl.order.price.total).toBe(25.5);

    });

    it('with 5% discount should be 25.5', function () {

      orderCtrl.order.price.discount = 0.05;
      orderCtrl.updateTotalPrice();
      expect(+orderCtrl.order.price.total.toFixed(2)).toBe(24.23);

    });

    it('with grade c and box area bigger than 2 m^2 should be 0', function () {

      orderCtrl.order.boxArea = 2.1;
      orderCtrl.order.grade = 'C';
      orderCtrl.validateOptions();
      expect(orderCtrl.order.price.total).toBe(0);

    });

    it('with grade b and reinforced bottom option should be 0', function () {

      orderCtrl.order.grade = 'B'
      orderCtrl.order.extras = ['Reinforced bottom'];
      orderCtrl.validateOptions();
      expect(orderCtrl.order.price.total).toBe(0);

    });

    it('with grade a and reinforced bottom option should be 25.5', function () {

      orderCtrl.order.grade = 'A'
      orderCtrl.order.extras = ['Reinforced bottom'];
      orderCtrl.validateOptions();
      expect(orderCtrl.order.price.total).toBe(25.5);

    });

  });

});