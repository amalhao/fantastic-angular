(function() {

  'use strict';

  angular.module('myApp.order', ['ngRoute'])

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/order', {
      templateUrl: 'order/order.html',
      controller: 'Order',
      controllerAs: 'orderVm'
    });
  }])

  // By changing this value to true you'll be able to dynamically check the total price of the order
  .constant('DYNAMIC_TOTAL_PRICE', false)

  .constant('MAX_AREA_FOR_GRADE_C', 2)

  // Used for the step navigation logic
  .constant('ORDER_STEPS', {
    dimensionsAndQuantity: 1,
    cardboardGrade: 2,
    printQuality: 3,
    optionalExtras: 4,
    totalCost: 5
  })

  // Dummy data - that sometimes might be retrieved from the server-side
  .constant('ORDER_OPTIONS',{
    grades: [
      {
        id: 1,
        name: 'A',
        price: 0.2
      },
      {
        id: 2,
        name: 'B',
        price: 0.1
      },
      {
        id: 3,
        name: 'C',
        price: 0.05
      }
    ],
    printings: [
      {
        id: 1,
        name: '3 colour printing',
        price: 0.2
      },
      {
        id: 2,
        name: '2 colour printing',
        price: 0.1
      },
      {
        id: 3,
        name: 'Black only printing',
        price: 0.05
      },
      {
        id: 5,
        name: 'No printing (plain cardboard)',
        price: 0.0
      },
      {
        id: 5,
        name: 'FantasticBoxCo',
        price: 0.0,
        discount: 0.05
      }
    ],
    extras: [
      {
        id: 1,
        name: 'Handles',
        price: 0.1
      },
      {
        id: 2,
        name: 'Reinforced bottom',
        price: 0.05,
        note: 'only available with grade A cardboard'
      }
    ]
  })

  // Injected lodash and the constants from above
  .controller('Order', ['_', 'DYNAMIC_TOTAL_PRICE', 'MAX_AREA_FOR_GRADE_C', 'ORDER_STEPS', 'ORDER_OPTIONS',
    function(_, dynamicTotalPrice, maxAreaForGradeC, orderSteps, orderOptions) {

      var vm = this;

      vm.dynamicTotalPrice = dynamicTotalPrice;
      vm.options = orderOptions;
      vm.maxAreaForGradeC = maxAreaForGradeC;
      vm.steps = orderSteps;
      vm.currentStep = vm.steps.dimensionsAndQuantity;

      // This object will store the data generated by the user for posterior manipulation
      vm.order = {
        dimensions: {
          width: 0.01,
          height: 0.01,
          length: 0.01
        },
        boxArea: 0,
        quantity: 0,
        grade: '',
        printing: '',
        extras: [],
        price: {
          grade: 0,
          printing: 0,
          extras: 0,
          discount: 0,
          total: 0
        }
      };
      vm.errors = [];

      vm.showStep = function(step) {
        vm.currentStep = step;
        if (step === vm.steps.totalCost) {
          vm.validateOptions();
        }
      };

      vm.nextStep = function() {
        vm.showStep(vm.currentStep + 1);
      };

      vm.previousStep = function() {
        vm.showStep(vm.currentStep - 1);
      };

      vm.updateBoxArea = function() {
        vm.order.boxArea = (2 * vm.order.dimensions.width * vm.order.dimensions.height)
            + (2 * vm.order.dimensions.length * vm.order.dimensions.height)
            + (2 * vm.order.dimensions.length * vm.order.dimensions.width);
        if (vm.dynamicTotalPrice) {
          vm.updateOrderGradePrintingExtrasPrices();
        }
      };

      vm.updateOrderGradePrintingExtrasPrices = function() {
        vm.order.price.grade = vm.order.boxArea * vm.order.quantity * getGradePrice();
        vm.order.price.printing = vm.order.boxArea * vm.order.quantity * getPrintingPrice();

        updateOrderExtrasPrice();
        if (vm.dynamicTotalPrice) {
          vm.updateTotalPrice();
        }
      };

      vm.updateOrderGradePrice = function() {
        vm.order.price.grade = vm.order.boxArea * vm.order.quantity * getGradePrice();
        if (vm.dynamicTotalPrice) {
          vm.updateTotalPrice();
        }
      };

      vm.updateOrderPrintingPrice = function() {
        vm.order.price.printing = vm.order.boxArea * vm.order.quantity * getPrintingPrice();
        vm.order.price.discount = getOrderPrintingDiscountPercentage();
        if (vm.dynamicTotalPrice) {
          vm.updateTotalPrice();
        }
      };

      vm.updateOrderExtrasPrice = function() {
        vm.order.price.extras = 0;
        _.forEach(vm.order.extras, function(extra) {
          var extraOption = _.find(vm.options.extras, { name: extra });
          vm.order.price.extras += extraOption.price * vm.order.quantity;
        });
      }

      vm.toggleExtras = function(extra) {
        var index = vm.order.extras.indexOf(extra);
        if (index > -1) {
          vm.order.extras.splice(index, 1);
        } else {
          vm.order.extras.push(extra);
        }

        vm.updateOrderExtrasPrice();
        if (vm.dynamicTotalPrice) {
          vm.updateTotalPrice();
        }
      };

      vm.updateTotalPrice = function() {
        vm.errors = [];
        vm.order.price.total = +(vm.order.price.grade + vm.order.price.printing + vm.order.price.extras).toFixed(2);
        if (vm.order.price.discount > 0) {
          vm.order.price.total -= vm.order.price.total * vm.order.price.discount;
        }
      };

      vm.showExtras = function() {
        var extrasString = '';
        for (var i = 0; i < vm.order.extras.length; i++) {
          extrasString += vm.order.extras[i];
          if (i < vm.order.extras.length - 1) {
            extrasString += ', '
          }
        }
        return extrasString;
      };

      vm.validateOptions = function() {
        vm.errors = [];
        if (vm.order.grade === 'C' && vm.order.boxArea > vm.maxAreaForGradeC) {
          vm.errors.push('The "Grade C" cardboard option is not available for boxes with a size of larger than ' +
            '2m^2. You can either change the box dimensions to lower values or change to a different grade.' +
            'The current box area is: ' + vm.order.boxArea.toFixed(2) + ' m^2.');
        }
        if (vm.order.grade !== 'A' && vm.order.extras.indexOf('Reinforced bottom') > -1) {
          vm.errors.push('The "Reinforced bottom" option is only available for Grade A cardboards. ' +
              'You can either change the cardboard to "Grade A" or un-select the "Reinforced bottom" ' +
              'option".');
        }
        if (vm.errors.length === 0) {
          vm.updateTotalPrice();
        }
      };

      // Gets the price for the selected grade from the dummy data
      function getGradePrice() {
        var grade = _.find(vm.options.grades, { name: vm.order.grade });
        return grade ? grade.price : 0;
      }

      // Gets the price for the selected printing from the dummy data
      function getPrintingPrice() {
        var printing = _.find(vm.options.printings, { name: vm.order.printing });
        return printing ? printing.price : 0;
      }

      // Gets the discount value if it exists
      function getOrderPrintingDiscountPercentage() {
        var printing = _.find(vm.options.printings, { name: vm.order.printing });
        return printing.discount ? printing.discount : 0;
      }

    }
  ]);

})();